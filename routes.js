module.exports = function(app) {

    //controle de versionamento
    const base = '/api/v1/';

    //--------------------------------//
    //        CHAMADA POR MÓDULO     //
    //-------------------------------//
    app.use(base + 'upload', require('./modulos/upload'));


    //-----------------------------//
    //         CHAMADA DEFAULT     //
    //-----------------------------//
    app.use(base, (req, res) => {
        res
            .status(404)
            .json(data = {
                'Title': 'Api Funil Pró',
                'Contact': 'contato@prosoftwares.net.br',
                'Status Code': '404'
            });
    });
    app.use('/', (req, res, next) => {
        res.redirect(base);        
    });
};