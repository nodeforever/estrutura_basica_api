/*
* @Author: Glauber Funez
* @Date:   2017-11-20 17:36:53
* @Last Modified by:   Glauber Funez
* @Last Modified time: 2017-11-20 17:37:08
*/


//--------------------------------//
//       gera error 400          //
//-------------------------------//
const error400 = (res, onde, msg) =>
  res.status(400).json({
    code: 400,
    data: {
      'erro': onde,
      'msg': msg
    }
  })

//--------------------------------//
//       gera error 401          //
//-------------------------------//
const error401 = (res, onde, msg) =>
  res.status(401).json({
    code: 401,
    data: {
      'erro': onde,
      'msg': msg
    }
  })


//--------------------------------//
//       gera error 403          //
//-------------------------------//
const error403 = (res, onde, msg) =>
  res.status(403).json({
    code: 403,
    data: {
      'erro': onde,
      'msg': msg
    }
  })


//--------------------------------//
//       gera error 404          //
//-------------------------------//
const error404 = (res, onde, msg) =>
  res.status(404).json({
    code: 404,
    data: {
      'erro': onde,
      'msg': msg
    }
  })



//--------------------------------//
//       gera error 500          //
//-------------------------------//
const error500 = (res, onde, msg) =>
  res.status(500).json({
    code: 500,
    data: {
      'erro': onde,
      'msg': msg
    }
  })



module.exports = {error400, error401, error403, error404, error500};