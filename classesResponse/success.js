/*
 * @Author: Glauber Funez
 * @Date:   2017-11-21 10:58:59
 * @Last Modified by:   Glauber Funez
 * @Last Modified time: 2018-01-19 10:16:41
 */

//--------------------------------//
//        success 200            //
//-------------------------------//
const success200 = (res, valor) => 
  res.status(200).json({
    code: 200,
    data: {
      response: valor
    }
  })

//--------------------------------//
//        success 201            //
//-------------------------------//
const success201 = (res, valor) =>
  res.status(201).json({
    code: 201,
    data: {
      response: valor
    }
  })


//--------------------------------//
//        success 202            //
//-------------------------------//
const success202 = (res, valor) =>
  res.status(202).json({
    code: 202,
    data: {
      response: valor
    }
  })


module.exports = {success200, success201, success202};