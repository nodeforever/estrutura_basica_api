/*
* @Author: Glauber Funez
* @Date:   2018-02-07 09:11:10
* @Last Modified by:   Glauber Funez
* @Last Modified time: 2018-02-07 09:11:40
*/

var express = require('express');
var router = express.Router();

router.post('/novo', require('./controller/novo')); // novo webhook

module.exports = router;