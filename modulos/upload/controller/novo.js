/*
* @Author: Glauber Funez
* @Date:   2018-02-07 09:11:54
* @Last Modified by:   Glauber Funez
* @Last Modified time: 2018-02-08 10:29:58
*/

const knex = require('./../../../config/knex');
const async = require('async');
const fs = require('fs');
const parse = require('csv-parse');
const classError = require('./../../../classesResponse/error.js');
const classSuccess = require('./../../../classesResponse/success.js');


const ControllerUpload = (req, res, next) => {
    
    const TokenFunil = req.headers.tokenfunil;

    if (!TokenFunil) {
        classError.error400(res, 1, 'Não foi enviado o Token');
    } else {

        /**
        * verifica token
        * recupera saas
        **/
        knex.select()
            .table('tb_Api')
            .where('api_token', TokenFunil)
            .then((result) => {

                const resSaasId = result[0].fk_saas_id;


                /**
                * Cria arquivo temporário CSV
                **/
                let splitBase64 = req.body['file'].split(';base64,').pop();
                fs.writeFile('./modulos/upload/tmp/fileImport_saas_'+ resSaasId +'.csv', splitBase64, {encoding: 'base64'}, function(err) {
                    console.log(err);
                });


                /**
                * Campos que podem conter no body
                **/
                const fileDelimiter = req.body['delimiter'];
                const fileEscape = req.body['escape'];


                /**
                * cria objeto de configuração
                **/
                const objOptions = {
                    columns: true
                }

                
                /**
                * verifica se foi passado o delimiter
                **/
                if ( fileDelimiter ) {
                    objOptions.delimiter = fileDelimiter;
                }


                /**
                * verifica se foi passado o escape
                **/
                if ( fileEscape ) {
                    objOptions.escape = fileEscape;
                }


                const parser = parse(objOptions, function(err, data){
                    res.json(data).end()
                })


                /**
                * Cria stream do file
                **/
                const readStreamFile = fs.createReadStream('./modulos/upload/tmp/fileImport_saas_'+ resSaasId +'.csv').pipe(parser);


            })
            .catch((err) => {
                classError.error500(res, 2, 'Erro ao consultar o token');
            });
    }    

};

module.exports = ControllerUpload;