const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors')


var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

// socket
app.use(function(req, res, next){
  res.io = io;
  next();
});

app.use(logger('dev'));
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
app.use(cookieParser());
app.use(cors()) //all request day 22/06/2017

//-----------------------------//
//       arquivo de rotas      //
//-----------------------------//
require('./routes')(app);

//---------------------------------------------------//
//       catch 404 and forward to error handler      //
//--------------------------------------------------//
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//--------------------------------------------//
//              error handlers               //
//           production error handler        //
//       no stacktraces leaked to user      //
//------------------------------------------//
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = {app: app, server: server};